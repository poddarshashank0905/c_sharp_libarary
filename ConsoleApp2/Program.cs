﻿using ClassLibrary2;


User userobj = new User();
//Below Code done by HardCoded

//userobj.id = 1;
//userobj.fname = "Shashank";
//userobj.lname = "Poddar";
//userobj.password = "12345";
//userobj.email = "shashankpoddar77@gmail.com";

//Below Code done by User Input

Console.WriteLine("Please Enter Your id");
userobj.id = int.Parse(Console.ReadLine());

Console.WriteLine("Enter First Name");
userobj.fname = Console.ReadLine();

Console.WriteLine("Enter List Name");
userobj.lname = Console.ReadLine();

Console.WriteLine("Enter Your Password");
userobj.password = Console.ReadLine();

Console.WriteLine("Please Enter your Email");
userobj.email = Console.ReadLine();


Console.WriteLine($"User ID::{userobj.id}\t User First Name::{userobj.fname}\t User Last Name::{userobj.lname}\t User Password::{userobj.password}\t User Email::{userobj.email}");

userobj.GetFullName();

Console.WriteLine(userobj.GetFullName());